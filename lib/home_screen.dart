import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'login_screen.dart';
import 'package:hexcolor/hexcolor.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  final TextEditingController _search = TextEditingController();

  Future<void> _signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    if (auth.currentUser != null) {
      print(auth.currentUser!.email);
    }
    return Scaffold(
      //backgroundColor: Colors.blueAccent
      body: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Container(
          margin: EdgeInsets.all(15.0),
          color: Colors.white,
          child: Column(
            children: [
              Container(
                height: 150,
                margin: EdgeInsets.only(bottom: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    ElevatedButton(
                      onPressed: () {
                        _signOut().then((value) => Navigator.of(context)
                            .pushReplacement(MaterialPageRoute(
                                builder: (context) => loginScreen())));
                      },
                      style:
                          ElevatedButton.styleFrom(primary: HexColor('FACE7F')),
                      child: Text(
                        'Logout',
                        style: TextStyle(color: HexColor('630A10')),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                //color: Colors.blueGrey,
                height: 140,
                margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text.rich(
                    TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: "Welcome, \n",
                          style: TextStyle(color: HexColor('FACE7F'))),
                      TextSpan(
                          text: auth.currentUser!.email,
                          style: TextStyle(color: HexColor('911F27')))
                    ]),
                    style: TextStyle(fontSize: 35),
                  ),
                ),
              ),
              Container(
                height: 75,
                margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
                child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Example : naruto',
                      contentPadding:
                          EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      prefixIcon: Icon(Icons.search),
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: HexColor('FACE7F'), width: 2.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: HexColor('630A10'), width: 2.0),
                      )
                      /*border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0)),
                            //fillColor: Colors.blue,*/
                      ),
                  //controller: _search,
                  onSubmitted: (_search) => Get.toNamed('/list_news',
                      arguments: 'api/search?search=${_search}'),
                ),
              ),
              Container(
                //color: Colors.lightGreen,
                height: 275,
                alignment: Alignment.topLeft,
                child: GridView.count(
                  crossAxisCount: 3,
                  childAspectRatio: 1.5,
                  mainAxisSpacing: 20,
                  crossAxisSpacing: 20,
                  padding: EdgeInsets.all(5),
                  children: <Widget>[
                    InkWell(
                      onTap: () => Get.toNamed('/list_news',
                          arguments: 'api/games?page=1'),
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.topLeft,
                            colors: [
                              HexColor('911F27'),
                              HexColor('630A10'),
                            ],
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Center(
                          child: Text(
                            'All Games News',
                            style: TextStyle(
                              color: HexColor('FACE7F'),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () => Get.toNamed('/list_news',
                          arguments: 'api/games/e-sport/?page=1'),
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.topLeft,
                            colors: [
                              HexColor('911F27'),
                              HexColor('630A10'),
                            ],
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Center(
                          child: Text(
                            'E-Sport News',
                            style: TextStyle(
                              color: HexColor('FACE7F'),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () => Get.toNamed('/list_news',
                          arguments: 'api/games/console-game?page=1'),
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.topLeft,
                            colors: [
                              HexColor('911F27'),
                              HexColor('630A10'),
                            ],
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Center(
                          child: Text(
                            'Console Games News',
                            style: TextStyle(
                              color: HexColor('FACE7F'),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () => Get.toNamed('/list_news',
                          arguments: 'api/tech?page=1'),
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.topLeft,
                            colors: [
                              HexColor('911F27'),
                              HexColor('630A10'),
                            ],
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Center(
                          child: Text(
                            'Tech News',
                            style: TextStyle(
                              color: HexColor('FACE7F'),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () => Get.toNamed('/list_news',
                          arguments: 'api/tech/tip?page=1'),
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.topLeft,
                            colors: [
                              HexColor('911F27'),
                              HexColor('630A10'),
                            ],
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Center(
                          child: Text(
                            'Tech Tips',
                            style: TextStyle(
                              color: HexColor('FACE7F'),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () => Get.toNamed('/aboutme'),
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topRight,
                            end: Alignment.topLeft,
                            colors: [
                              HexColor('911F27'),
                              HexColor('630A10'),
                            ],
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Center(
                          child: Text(
                            'About Me',
                            style: TextStyle(
                              color: HexColor('FACE7F'),
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
