import 'dart:convert';

DetailnewsModel detailnewsModelFromJson(String str) => DetailnewsModel.fromJson(json.decode(str));

String detailnewsModelToJson(DetailnewsModel data) => json.encode(data.toJson());

class DetailnewsModel {
    DetailnewsModel({
        required this.method,
        required this.status,
        required this.results,
    });

    String method;
    bool status;
    Results results;

    factory DetailnewsModel.fromJson(Map<String, dynamic> json) => DetailnewsModel(
        method: json["method"],
        status: json["status"],
        results: Results.fromJson(json["results"]),
    );

    Map<String, dynamic> toJson() => {
        "method": method,
        "status": status,
        "results": results.toJson(),
    };
}

class Results {
    Results({
       required this.title,
       required this.thumb,
       required this.author,
       required this.date,
       required this.categories,
       required this.figure,
       required this.content,
    });

    String title;
    String thumb;
    String author;
    String date;
    List<String> categories;
    List<String> figure;
    List<String> content;

    factory Results.fromJson(Map<String, dynamic> json) => Results(
        title: json["title"],
        thumb: json["thumb"],
        author: json["author"],
        date: json["date"],
        categories: List<String>.from(json["categories"].map((x) => x)),
        figure: List<String>.from(json["figure"].map((x) => x)),
        content: List<String>.from(json["content"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "title": title,
        "thumb": thumb,
        "author": author,
        "date": date,
        "categories": List<dynamic>.from(categories.map((x) => x)),
        "figure": List<dynamic>.from(figure.map((x) => x)),
        "content": List<dynamic>.from(content.map((x) => x)),
    };
}
