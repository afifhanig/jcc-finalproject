import 'dart:math';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'home_screen.dart';
import 'package:get/get.dart';

class loginScreen extends StatefulWidget {
  @override
  _loginScreenState createState() => _loginScreenState();
}

class _loginScreenState extends State<loginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        SystemNavigator.pop();
        return Future.value(false);
      },
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        //backgroundColor: HexColor('FCF0C8'),
        body: SingleChildScrollView(
          reverse: true,
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Container(
                  height: 100,
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 200,
                        decoration: BoxDecoration(
                            //color: Colors.red,
                            image: DecorationImage(
                                image: AssetImage('assets/images/Logo2.png'),
                                fit: BoxFit.contain)),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: 250,
                  //color: Colors.deepPurple,
                  child: Column(
                    children: [
                      TextField(
                        decoration: InputDecoration(
                            hintText: "Email",
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: HexColor('FACE7F'), width: 2.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: HexColor('630A10'), width: 2.0),
                            )),
                        controller: _emailController,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                            hintText: "Password",
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: HexColor('FACE7F'), width: 2.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: HexColor('630A10'), width: 2.0),
                            )),
                        controller: _passwordController,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      // Text(
                      //   "Forgot Password?",
                      //   style: TextStyle(fontSize: 18, color: Colors.blue[300]),
                      // ),
                      SizedBox(
                        height: 20,
                      ),
                      ElevatedButton(
                        onPressed: () async {
                          try {
                            await _firebaseAuth
                                .signInWithEmailAndPassword(
                                    email: _emailController.text,
                                    password: _passwordController.text)
                                .then(
                                  (value) => Get.off(() => Homepage())
                                );
                          } catch (e) {
                            print(e.toString());
                            final snackBar =
                                SnackBar(content: Text(e.toString()));
                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          }
                          ;
                        },
                        child: Text(
                          'Login',
                          style: TextStyle(color: HexColor('630A10')),
                        ),
                        style: ElevatedButton.styleFrom(
                            fixedSize: Size(340, 40),
                            primary: HexColor('FACE7F')),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Doesn't Have an Account? \n Fill Form Above Than Hit ->",
                            style: TextStyle(fontSize: 16),
                          ),
                          TextButton(
                            onPressed: () async {
                              try {
                                await _firebaseAuth
                                    .createUserWithEmailAndPassword(
                                        email: _emailController.text,
                                        password: _passwordController.text);
                              } catch (e) {
                                print(e.toString());
                                final snackBar =
                                    SnackBar(content: Text(e.toString()));
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(snackBar);
                              }
                            },
                            child: Text(
                              "\nSign Up",
                              style: TextStyle(
                                  fontSize: 16, color: HexColor('630A10')),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
