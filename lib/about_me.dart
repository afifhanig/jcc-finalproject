import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:url_launcher/url_launcher.dart';

class aboutMe extends StatefulWidget {
  @override
  _aboutMeState createState() => _aboutMeState();
}

class _aboutMeState extends State<aboutMe> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text(
          'About Me',
          style: TextStyle(
            color: HexColor('FCF0C8'),
          ),
        ),
        backgroundColor: HexColor('630A10'),
      ),
      body: Column(
        children: [
          Container(
            height: 100,
            //color: Colors.green,
          ),
          Container(
            padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
            height: 180,
            //color : Colors.red,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  'Afif Hani Ghuswari',
                  style: TextStyle(fontSize: 20, color: HexColor('FACE7F')),
                ),
                SizedBox(
                  width: 10,
                ),
                CircleAvatar(
                  backgroundColor: HexColor('FACE7F'),
                  radius: 55,
                  child: CircleAvatar(
                    radius: 50,
                    backgroundImage: AssetImage('assets/images/Foto1.png'),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            thickness: 3,
            color: HexColor('911F27'),
            endIndent: 30,
            indent: 30,
          ),
          Container(
            padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
            height: 300,
            //color: Colors.blue,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    InkWell(
                      onTap: () => _launchURL('https://www.facebook.com/afifhanig/'),
                      child: Image(
                        image: AssetImage('assets/icon/FB Logo.png'),
                        height: 50,
                        width: 50,
                      ),
                    ),
                    SizedBox(width: 10),
                    Text("afifhanig")
                  ],
                ),
                Row(
                  children: [
                    Image(
                      image: AssetImage('assets/icon/Gmail Logo.png'),
                      height: 50,
                      width: 50,
                    ),
                    SizedBox(width: 10),
                    Text("afifhanighuswari@gmail.com")
                  ],
                ),
                Row(
                  children: [
                    InkWell(
                      onTap:() => _launchURL('https://www.instagram.com/afifhanig/'),
                      child: Image(
                        image: AssetImage('assets/icon/IG Logo.png'),
                        height: 50,
                        width: 50,
                      ),
                    ),
                    SizedBox(width: 10),
                    Text("@afifhanig")
                  ],
                ),
                Row(
                  children: [
                    InkWell(
                      onTap:() => _launchURL('https://gitlab.com/afifhanig'),
                      child: Image(
                        image: AssetImage('assets/icon/Gitlab Logo.png'),
                        height: 50,
                        width: 50,
                      ),
                    ),
                    SizedBox(width: 10),
                    Text("@afifhanig")
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
