import 'package:get/get.dart';
import 'detailnews_model.dart';

class Controller extends GetxController {
  RxMap detailNews = {}.obs;
  void setNews(Map _val) {
    detailNews.value = _val;
  }

  RxList listNews = [].obs;
  void setListNews(List _val) {
    listNews.value = _val;
  }
}