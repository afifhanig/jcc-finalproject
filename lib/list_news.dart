import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:hexcolor/hexcolor.dart';
import 'controller.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'controller.dart';

class listNews extends StatefulWidget {
  @override
  _listNewsState createState() => _listNewsState();
}

class _listNewsState extends State<listNews> {
  final controller = Get.put(Controller());
  var news;

  Future<void> getNews() async {
    var url =
        Uri.parse('https://the-lazy-media-api.vercel.app/${Get.arguments}');
    var response = await http.get(url);
    news = jsonDecode(response.body);
    controller.setListNews(news);
    //print(news);
  }

  @override
  void initState() {
    getNews();
    super.initState();   
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text(
          'List of News',
          style: TextStyle(
            color: HexColor('FCF0C8'),
          ),
        ),
        backgroundColor: HexColor('630A10'),
      ),
      body: Obx(
        () => controller.listNews.length > 0
            ? _myListView(context)
            : const Center(child: spinkit),
      ),
    );
  }
}

Widget _myListView(BuildContext context) {
  final controller = Get.put(Controller());

  return ListView.builder(
    itemCount: controller.listNews.length,
    itemBuilder: (context, index) {
      return InkWell(
        onTap: () => Get.toNamed('/detailnews',
            arguments: [controller.listNews[index]['key'], controller.listNews[index]['thumb']]),
        child: Card(
          color: HexColor('FCF0C8'),
          child: ListTile(
            leading: Image(
              image: NetworkImage(controller.listNews[index]['thumb']),
            ),
            contentPadding: EdgeInsets.all(10.0),
            title: Text(controller.listNews[index]['title']),
            subtitle: Text(
              controller.listNews[index]['desc'],
              overflow: TextOverflow.ellipsis,
              maxLines: 3,
            ),
          ),
        ),
      );
    },
  );
}

const color = const Color(0xff911F27);
const spinkit = SpinKitFadingGrid(
  color: color,
  size: 50.0,
);
