import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
//import 'controller.dart';
import 'package:http/http.dart' as http;
import 'package:get/get.dart';
import 'dart:convert';
import 'detailnews_model.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'controller.dart';

class DetailNews extends StatefulWidget {
  @override
  _DetailNewsState createState() => _DetailNewsState();
}

class _DetailNewsState extends State<DetailNews> {
  final controller = Get.put(Controller());

  Future<void> getNews() async {
    var url = Uri.parse(
        'https://the-lazy-media-api.vercel.app/api/detail/${Get.arguments[0]}');
    var response = await http.get(url);
    var news = jsonDecode(response.body);
    controller.setNews(news['results']);
    print(Get.arguments[1]);
  }

  @override
  void initState() {
    getNews();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text(
          'Detail News',
          style: TextStyle(
            color: HexColor('FCF0C8'),
          ),
        ),
        backgroundColor: HexColor('630A10'),
      ),
      body: Obx(
        () => controller.detailNews.length > 0
            ? myWidget(context)
            : const Center(child: spinkit),
      ),
    );
  }
}

Widget myWidget(BuildContext context) {
  final controller = Get.put(Controller());
  var panjang = controller.detailNews['content'].length;
  return Padding(
    padding: EdgeInsets.all(10.0),
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(controller.detailNews['author']),
            Text(controller.detailNews['date']),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          height: 230,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(Get.arguments[1]),
              fit: BoxFit.fill,
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Title(
          color: Colors.black,
          child: Text(
            controller.detailNews['title'],
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
            textAlign: TextAlign.justify,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Expanded(
          child: ListView.builder(
            itemCount: panjang,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: Text(
                  controller.detailNews['content'][index == 0 ? index + 1 : index],
                  style: TextStyle(fontSize: 16),
                ),
                //subtitle: Text(controller.detailNews[index]['value']),
              );
            },
          ),
        ),
      ],
    ),
  );
}

const color = const Color(0xff911F27);
const spinkit = SpinKitFadingGrid(
  color: color,
  size: 50.0,
);
