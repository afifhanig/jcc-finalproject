import 'package:final_project/detail_news.dart';
import 'package:final_project/list_news.dart';
import 'package:final_project/home_screen.dart';
import 'package:final_project/login_screen.dart';
import 'package:final_project/about_me.dart';
import 'package:final_project/splashscreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: '/',
      getPages: [
        GetPage(name: '/', page: () => Homepage()),
        GetPage(name: '/loginscreen', page: () => loginScreen()),
        GetPage(name: '/detailnews', page: () => DetailNews()),
        GetPage(name: '/list_news', page: () => listNews()),
        GetPage(name: '/aboutme', page: () => aboutMe())
      ],
      title: 'Your Gaming News',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: loginScreen(),
    );
  }
}